package br.com.mauda.seminario.cientificos.model;

import br.com.mauda.seminario.cientificos.model.enums.SituacaoInscricaoEnum;

public class Inscricao {

    private Estudante estudante;
    private Seminario seminario;
    private SituacaoInscricaoEnum situacao = SituacaoInscricaoEnum.DISPONIVEL;

    private Long id;
    private boolean direitoMaterial;

    public Inscricao(Seminario seminario2) {
        this.seminario = seminario2;
        this.seminario.getInscricoes().add(this);
    }

    public Estudante getEstudante() {
        return this.estudante;
    }

    public void setEstudante(Estudante estudante) {
        this.estudante = estudante;
    }

    public Seminario getSeminario() {
        return this.seminario;
    }

    public void setSeminario(Seminario seminario) {
        this.seminario = seminario;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public boolean getDireitoMaterial() {
        return this.direitoMaterial;
    }

    public void setDireitoMaterial(boolean direitoMaterial) {
        this.direitoMaterial = direitoMaterial;
    }

    public void comprar(Estudante estudante2, Boolean direitoMaterial2) {
        this.setEstudante(estudante2);
        this.estudante.getInscricoes().add(this);
        this.setDireitoMaterial(direitoMaterial2);
        this.situacao = SituacaoInscricaoEnum.COMPRADO;
    }

    public void realizarCheckIn() {
        this.setSituacao(SituacaoInscricaoEnum.CHECKIN);
    }

    public SituacaoInscricaoEnum getSituacao() {
        return this.situacao;
    }

    public void setSituacao(SituacaoInscricaoEnum situacao) {
        this.situacao = situacao;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + (this.id == null ? 0 : this.id.hashCode());
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (this.getClass() != obj.getClass()) {
            return false;
        }
        Inscricao other = (Inscricao) obj;
        if (this.id == null) {
            if (other.id != null) {
                return false;
            }
        } else if (!this.id.equals(other.id)) {
            return false;
        }
        return true;
    }

}
