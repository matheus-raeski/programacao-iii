package br.com.mauda.seminario.cientificos.model.enums;

public enum SituacaoInscricaoEnum {
    COMPRADO(1L, "disponivel"),
    CHECKIN(2L, "comprado"),
    DISPONIVEL(3L, "checkIn");

    private Long id;
    private String nome;

    private SituacaoInscricaoEnum(Long id, String nome) {
        this.id = id;
        this.nome = nome;
    }

    public Long getId() {
        return this.id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNome() {
        return this.nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

}
